package testapi;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class UserUpdatePatch {
	
	//Test verifies response for code and schema on user update by Patch.
	//Code 200 means success.
	@Parameters({"URL", "name", "job", "endpoint"})
	@Test
	public void userCanBeUpdatedWithPatch(String Uri, String name, String job, String endpoint){
			given()
		   .baseUri(Uri).body("{'name':"+name+",'job':"+job+"}")
		   .when()
		   .patch(endpoint)
		   .then()
		   .assertThat()
		   .statusCode(200).body(matchesJsonSchemaInClasspath("userUpdatedByPatch.json"));
	   }
}
