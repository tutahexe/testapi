package testapi;


import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class UserCreate {
	
	//Test verifies response for code and schema on create user request.
	//Response code 201 means 'Created'.
	@Parameters({"URL", "name", "job", "endpoint"})
	@Test
	public void userCanBeCreated(String Uri, String name, String job, String endpoint){
			given()
		   .baseUri(Uri).body("{'name':"+name+",'job':"+job+"}")
		   .when()
		   .post(endpoint)
		   .then()
		   .assertThat()
		   .statusCode(201).body(matchesJsonSchemaInClasspath("userCreated.json"));
	}
}
