package testapi;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class UserDeleteById {

		//Test validates that user can be deleted.
		//204 means success, nothing to send in response body
		@Parameters({"URL", "endpoint"})
		@Test
		public void userCanBeDeletedById(String Uri, String endpoint){
				given()
			   .baseUri(Uri)
			   .when()
			   .delete(endpoint)
			   .then()
			   .assertThat()
			   .statusCode(204);
	}
}