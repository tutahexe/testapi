package testapi;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class UserGetById {
	
	//Test verifies response for code and schema on user info receive.
	//Code 200 means success.
	@Parameters({"URL", "endpoint"})
	@Test
	public void userDataCanBeReceivedById(String Uri, String endpoint){
			given()
		   .baseUri(Uri)
		   .when()
		   .get(endpoint)
		   .then()
		   .assertThat()
		   .statusCode(200).body(matchesJsonSchemaInClasspath("userInfoById.json"));
	}
}
