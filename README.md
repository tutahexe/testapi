# TestAPI

TestAPI is rest-assured based automation framework used for testing 'https://reqres.in/' API.

## Installation
Requirements:
* MAVEN
* JAVA 1.8

Use MAVEN to install all dependencies.

```
mvn install
```

## Execution configuration

## Test variables
All test variables can be configured by test NG XMl.
For instance: 
Base URL, user details or endpoint(which includes ID to specify user for action).

## Single thread / multi-thread configuration

Edit testng.xml to configure number of threads used in run.

##### For single thread run:
testng.xml

```
<suite name="Suite">
```
##### For multi-thread run:
testng.xml

```
<suite name="Suite" parallel="classes" thread-count="n">
```
Where n - number of threads

## Run

Use MAVEN to build and run tests.

```MVN
mvn clean test
```

## Report
Once test execution is finished - report can be found in 

```
'/test-output/index.html'
```